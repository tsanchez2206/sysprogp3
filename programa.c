/*Mi primer programa en c*/
#include<stdio.h>
#include<stdlib.h>
#include<strings.h>

void clear();
float calcular_promedio_edad(int edad[]); 

void main()
{
	char nombres[10][50];
	char apellidos[10][50];
	int edad[10];
	int x=0;
	inicializar_arreglo(nombres);
	inicializar_arreglo(apellidos);
	printf("A continuacion ingresara los siguientes datos requeridos:\n\n");
	for (x=0;x<10;x++)
	{
		printf("\n---------------------------------------------------------------\n");
		printf("\n");
		do
		{
			printf("\tIngrese el nombre de la persona %i: ",x+1);
			scanf("%s",nombres[x]);
		}while(validar_nombre_apellido(nombres[x])==0);
		do
		{
			getchar();
			printf("\tIngrese el apellido de la persona %i: ",x+1);
			scanf("%s",apellidos[x]);
		}while(validar_nombre_apellido(apellidos[x])==0);

		do
		{
			
			printf("\tIngrese la edad de la persona %i: ",x+1);
			scanf("%i",&edad[x]);
			getchar();
                /*La siguiente linea valida que la edad ingresada sea un numero entre 0 y 130*/
		}while(edad[x]<0 || edad[x]>130);
	}
	printf("\nCalculando...\n\n"); 
        /*La siguiente linea muestra el promedio de las edades*/
	printf("El promedio de edad es: %f",calcular_promedio_edad(edad));
	printf("\n");
        /*Las siguientes lineas muestran la persona mayor y menor con su edad respectiva*/
	persona_mayor(nombres,apellidos,edad);
	persona_menor(nombres,apellidos,edad);

}
void clear()
{
	getchar();
	getchar();
}

/*Funcion que permite calcular el promedio de la edad*/
float calcular_promedio_edad(int edad[])
{
	float acumulador=0;
	float promedio=0;
	int x=0;
	for(x=0;x<10;x++)
	{
		acumulador+=edad[x];
	}
	promedio=acumulador/10.0;
	return promedio;
}

/*Funcion que permite encontrar quien es la persona mayor*/
void persona_mayor(char nombres[][50],char apellidos[][50],int edad[])
{
	int edad_mayor=0;
	int index_mayor=0;
	int x=0;
	for(x=0;x<10;x++)
	{
		if(edad[x]>edad_mayor)
		{
			index_mayor=x;
			edad_mayor=edad[x];
		}
	}
	printf("\nLa persona con mayor edad: %s %s con edad: %i\n",nombres[index_mayor],apellidos[index_mayor],edad[index_mayor]);
}

/*Funcion que permite encontrar quien es la persona menor*/
void persona_menor(char nombres[][50],char apellidos[][50],int edad[])
{
	int edad_menor=200;
	int index_menor=0;
	int x=0;
	for(x=0;x<10;x++)
	{
		if(edad[x]<edad_menor)
		{
			index_menor=x;
			edad_menor=edad[x];
		}
	}
	printf("\nLa persona con menor edad: %s %s con edad: %i\n",nombres[index_menor],apellidos[index_menor],edad[index_menor]);
}

/*Funcion que permite validar que el nombre y apellido ingresados no contengan caracteres
especiales y empiecen con mayuscula*/
int validar_nombre_apellido(char cadena[])
{
	char c=cadena[0];
	int z=1;
	if (c>='A' && c<='Z')
	{

		for(z=1;z<strlen(cadena);z++)
		{

			if(cadena[z]<97 || cadena[z]>122)
			{
				return 0;
			}
		}
		return 1;
	}
	else
	{
		return 0;
	}
}
void inicializar_arreglo(char arr[][50])
{
	int x=0;
	int y=0;	
	for(x=0;x<10;x++)
	{
		for(y=0;y<50;y++)
		{
			arr[x][y]="";
		}
	}
}
